# meffie.de

Dieses Repository gehört zu meiner [Website](https://www.meffie.de), welche hauptsächlich zu Dokumentationszwecken dient.

Verwendetes Hugo Template: [Hextra](https://github.com/imfing/hextra) von [Xin Fu](https://github.com/imfing).

## Setup

1. Hugo [herunterladen](https://github.com/gohugoio/hugo/releases) und installieren

2. Seite [lokal](http://localhost:1313) hosten
```
hugo server
```

## ToDo
- eigene Bilder vom Fahrradequipment machen
