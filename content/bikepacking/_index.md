---
title: Bikepacking
cascade:
  type: docs
---

Nach meinem Studium möchte ich eine große Radtour durch Europa machen. Bis dahin werden hier nur ein paar kleinere Touren auftauchen.

{{< cards >}}
  {{< card link="equipment" title="Ausrüstung" image="/images/bike.jpg" subtitle="Meine Ausrüstung" >}}
{{< /cards >}}


## Geplante Touren

{{< cards >}}
  {{< card link="https://www.komoot.com/de-de/collection/1077286/radpilgern-auf-der-eurovelo-3-norwegen-bis-frankreich" title="Eurovelo 3" subtitle="Von Deutschland an die französisch-spanische Grenze">}}
  {{< card link="https://www.komoot.com/de-de/collection/1080662/radpilgern-in-spanien-camino-frances-und-camino-finisterre" title="Jakobsweg in Spanien" subtitle="Camino Francés und Camino Finisterre">}}
{{< /cards >}}