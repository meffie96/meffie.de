---
title: Ausrüstung
---

## Fahrrad: Bergamont Grandurance RD 3
![Bergamont Grandurance RD 3](/images/bike.jpg "Bergamont Grandurance RD 3")
- **Rahmen**: 28", super lite AL-6061 Rohrsatz
- **Gabel**: Grandurance Aluminium, Schnellspanner, Flatmount-Aufnahme, drei Befestigungsösen pro Seite
- **Schaltwerk**: Shimano Claris, RD-R2000, long cage
- **Umwerfer**: Shimano Claris, FD-R2000
- **Schalthebel**: Shimano Claris, ST-R2000, 2x8-fach, road STI-Schalthebel
- **Kurbelsatz**: Shimano FC-RS200, 50/34t
- **Kassette**: Shimano CS-HG31-8, 11-34t
- **Bremsen**: Tektro MD-C400, mechanische Scheibenbremse
- **Lenker**: Satori X-Race, Flare: 16°
- **Vorbau**: Syncros 3.0, +/-10°
- **Sattelstütze**: Syncros
- **Sattel**: Syncros Tofino 2.5
- **Vorderradnabe**: Shimano Nabendynamo, DH-3D32, 6-Loch, Disc, Schnellspanner
- **Hinterradnabe**: BGM Pro, 6-Loch, Disc, Schnellspanner
- **Felgen**: Shining DB-T260, Disc
- **Reifen**: Schwalbe G-One Allround, RaceGuard, 40-622
- **Beleuchtung vorne**: Herrmans H-Black MR4, LED, Standlicht
- **Beleuchtung hinten**: Herrmans H-Trace Mini, LED, Standlicht
- **Gepäckträger**: Racktime / BGM Allroad Carrier, SnapIt 2.0 System
- **Pedale**: Acid Combo A4-IB Hybrid Pedale
- **Gewicht**: 13,2 kg


## Taschen
- **Lenkertasche**: Vaude Trailfront II
- **Rahmentasche**: Blackburn Outpost Elite Frame Bag Large
- **Gepäckhalter (2x)**: Blackburn Outpost Cargo Cage
- **Oberrohrtasche**: Riverside ACTV 100 1,5 Liter
- **Snackpouch (2x)**: Riverside Food Pouch ADTV 900
- **Rucksack**: ?


## Kleidung
- Giro Helm
- Rest muss ich mir noch überlegen


## Schlafaustattung
- Tunnelzelt Forclaz MT900 Ultralight
- Schlafsack
- Isomatte
- Zahnbürste, Zahnpasta, Rasierer, Deo
- Mikrofaserhandtuch


## Werkzeug
- Letherman
- Öl
- Plattenflickzeug
- Ersatzschlauch
- Panzertape


## Elektronik
- OnePlus 7 Pro, am Lenker montiert mit SP Connect Micro Stem Mount
- Fahrradlader V3.1
- Bluetoothtracker
- Ladekabel
- Netzteil
- SunnyBag Powerbank mit 10.000 mAh
- Tolino Shine 3
- Kopfhörer


## Essen & Trinken
- Campingkocher (Alkohol / Woodburner)
- Geschirr & Besteck
- Camelbak Poudium Steel Bike Bottle 650ml
- Zusätzliche Wasserflaschen an der Gabel


## Krimskrams
- Schloss ABUS Catena 6806K/110
- Luftpumpe Lezyne Micro Floor Drive HV
- Geld und Karten
- Schere
- Nagelfeile
- Pinzette
- Brille
- Tagebuch, Stift, Radiergummi
- Klopapier
- Minispaten?
- Medizinische Ausstattung (Ibuprofen, Tape)?
- Sonnencreme
- Wäscheleine