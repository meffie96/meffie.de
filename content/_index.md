---
title: "meffie.de"
toc: false
---

Willkommen auf meiner Website!

## Quicklinks

{{< cards >}}
  {{< card link="docs" title="Dokumentation" icon="document" subtitle="Dokumentation meiner aktuellen IT-Projekte." >}}
  {{< card link="blog" title="Archiv" icon="archive" subtitle="Nicht mehr aktuelle Dokumentationen." >}}
  {{< card link="bikepacking" title="Bikepacking" icon="map" subtitle="Hier geht's zu meinen Radreisen." >}}
  {{< card link="books" title="Leseecke" icon="book-open" subtitle="Eine Sammlung lesenswerter Bücher." >}}
{{< /cards >}}

## Über mich {{< icon "user" >}}

Hi!

Ich habe eine Ausbildung zum Fachinformatiker für Systemintegration gemacht und studiere aktuell Informatik an der [Universität Münster](https://www.uni-muenster.de/de/). Ich interessiere mich für Docker, Smarthome und Bikepacking. :)