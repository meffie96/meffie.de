---
title: Computherm Q20 an Vaillant Thermoblock VCW 180 XEU
date: 2024-04-02
authors: 
  - name: Stefan
    link: https://gitlab.com/meffie96
    image: https://gitlab.com/uploads/-/system/user/avatar/4374806/avatar.png
tags:
  - smart home
  - heizung
  - vaillant
weight: 10
---

Der Hauptgrund für den Austausch des Raumthermostats ist seine laut tickende Uhr. Als Ersatz habe ich mir das digitale Computherm Q20 besorgt. Es lässt sich zwar nicht über WLAN steuern, dafür aber recht fein einstellen und hat vor allem keine tickende Uhr.

```goat
.-------------------------------------------.                                    
|     Vaillant Thermoblock VCW 180 XEU      |
|                                           |
|   N   L   1   2   3   4   5   7   8   9   |
.-----------+---+---+---+-------------------.
            |   |   |   |
            N   L   |   |
                    |   |          
                .---+---+--------.
                |   NO COM NC    |
                |                |
                | Computherm Q20 |
                .----------------.
```
*Anschluss des Computherm Q20 Raumthermostats an den Thermoblock.*