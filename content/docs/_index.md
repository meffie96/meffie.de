---
title: "Dokumentation"
weight: 1
---

Hier dokumentiere ich meine aktuellen IT-Projekte. Das ist zwar primär für mich selbst, aber vielleicht nutzt es ja der ein oder anderen Person. :)


{{< cards >}}
  {{< card link="pi" title="Raspberry Pi" icon="chip" >}}
  {{< card link="workstation" title="Workstation" icon="terminal" >}}
  {{< card link="smarthome" title="Smarthome" icon="home" >}}
{{< /cards >}}