---
title: "Linux-Workstation"
weight: 1
---

Ich habe auf meinem PC Dual-Boot mit Windows 10 und LMDE (Linux Mint Debian Edition) eingerichtet. Die Betriebssysteme sind auf zwei verschiedenen SSDs installiert, hauptsächlich um die Wartung zu vereinfachen und damit sich die Bootloader nicht in die Quere kommen.


## Datensicherung
Die eigentliche Installation habe ich nicht abgesichert, da ich jederzeit neu installieren kann, ohne Daten zu verlieren. Das liegt vor allem daran, dass ich meine wichtigen Daten wie Dokumente und Fotos auf meinem NAS ablege. Da ich die Linux-Installation ausschließlich zum Arbeiten verwende, sei es nun für private oder dienstliche Zwecke, müssen lediglich git Repositories gesichert werden. Da diese alle auf [gitlab](https://gitlab.com) oder [github](https://github.com) liegen, ist das mit einem einfachen `git push` erledigt.

## Passwortverwaltung
Das Mittel der Wahl ist für mich [KeePassXC](https://keepassxc.org/) beziehungsweise [Keepass2Android](https://play.google.com/store/apps/details?id=keepass2android.keepass2android&pli=1). Die Synchronisation der Datenbank über all meine Geräte erledige ich mithilfe einer von meiner Uni gehosteten OwnCloud-Instanz. Für KeePass gibt es auch eine sehr praktische Browser-Erweiterung, die es ermöglicht, Anmeldedaten automatisch auszufüllen.

## Everything AI
### Ollama
Starte Ollama mit Docker:

> docker run -d --gpus=all --restart=always -v $HOME/.ollama:/root/.ollama -p 11434:11434 --name ollama ollama/ollama

Ollama läuft nun immer im Hintergrund, auch nach einem Neustart des Rechners.

Lade ein Modell mit Ollama herunter:
```
docker exec -ti ollama pull deepseek-coder:6.7b-base
docker exec -ti ollama pull deepseek-coder:6.7b-instruct
```

### VS-Code Plugin Privy
1. Installation über den VS-Code Marketplace
2. In den Einstellungen des Plugins deepseek-coder als model setzen.