---
title: "Heizung"
---

Wir haben einen recht alten Vaillant Thermoblock VCW 180 XEU bei uns im Badezimmer und ein Vaillant VRT-QZA Raumthermostat in einem anderen Raum. An dem Raumthermostat lässt sich eine Komforttemperatur einstellen und zu welchen Zeiten diese gehalten werden soll. Ansonsten ist werksseitig eingestellt, dass 15°C gehalten werden sollen. Ist die Temperatur in dem Raum unterhalb der zu haltenden Temperatur, wird eine Heizungsanforderung an den Thermoblock gestellt, woraufhin sich dieser einschaltet, bis die Raumtemperatur wieder ihrem gewünschten Wert erreicht hat. Ob die anderen Räume der Wohnung dabei warm genug werden ist in unserem Fall mehr oder weniger Glückssache, da der mit dem Thermostat ausgestattete Raum der erste im Heizkreislauf ist.

```goat
.-------------------------------------------.                                    
|     Vaillant Thermoblock VCW 180 XEU      |
|                                           |
|   N   L   1   2   3   4   5   7   8   9   |
.-----------+---+---+---+-------+---+---+---.
            |   |   |   |       |   |   |
            N   L   .---.   .---+---+---+------.
                            |   7   8   9      |
                            |                  |
                            | Vaillant VRT-QZA |
                            .------------------.
```
*Das Raumthermostat ist über die Klemmen 7, 8 und 9 mit dem Thermoblock verbunden. Zusätzlich sind die Klemmen 3 und 4 mit einem kurzen Kabel überbrückt.*


## Steuerung des Thermoblocks per WLAN
Um die Heizung so richtig smart zu machen verwende ich einen [Shelly 1](https://www.shelly.com/de/products/shop/shelly-1) sowie jeweils ein [Fritz!Dect 301 Heizkörperthermostat](https://avm.de/produkte/smart-home/fritzdect-301/) für jeden Raum.

```goat
.-------------------------------------------.                                    
|     Vaillant Thermoblock VCW 180 XEU      |
|                                           |
|   N   L   1   2   3   4   5   7   8   9   |
.---+---+---+---+---+---+-------------------.
    |   |   |   |   |   |
    |   |   N   L   |   |
    |   |           |   |
    |   .-----------(---(-----------.
    |               |   |           |
    .---------------(---(-------.   |
                    |   |       |   |
                 .--+---+-------+---+--.
                 |  O   I   SW  N   L  |
                 |                     |
                 |       Shelly 1      |
                 .---------------------.
```
*Anschluss eines Shelly 1 an den Thermoblock.*

Zusammen mit Homeassistant ist es so möglich, für jeden Raum individuell eine Temperatur einzustellen und diese automatisch anzupassen, je nachdem wer gerade zuhause ist. Sobald ein Raum Wärme anfordert, kann der Shelly die Heizung einschalten, unabhängig davon wie warm es im Thermostat-Raum gerade ist. Außerdem lässt sich eine Automatisierung einrichten, die die Raumtemperatur im Schlafzimmer über Nacht absenkt. Dazu lässt sich prima der Handywecker auslesen und z.B. 8 Stunden vor dem nächsten Klingeln die Temperatur auf 18°C einstellen.


## Homeassistant + Fritz!Dect 301
Um die eingestellte Zieltemperatur und aktuelle Temperatur in Automatisierungen nutzen zu können, kann unter Helpers ein Template angelegt werden. Dabei können die folgenden Attribute genutzt werden:
```
{{ states.climate.kitchen.attributes.temperature }} ## Zieltemperatur
{{ states.climate.kitchen.attributes.current_temperature }} ## Aktuelle Temperatur
{{ states.climate.kitchen.attributes.current_temperature - states.climate.kitchen.attributes.temperature }} ## Temperaturdifferenz
```

Um die Temperaturdifferenz in einer Automatisierung nutzen zu können, lässt sich unter `Helpers -> Template -> Template a sensor` ein neuer Sensor anlegen und dem entsprechenden Thermostat zuordnen.