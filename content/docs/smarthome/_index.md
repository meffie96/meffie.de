---
title: "Smart Home"
weight: 1
---

Für mein Smart Home verwende ich einen Home Assistant Container, welcher auf meinem QNAP-NAS läuft.

## Homeassistant auf QNAP Container Station
```
version: '3'
services:
  homeassistant:
    container_name: homeassistant
    image: "ghcr.io/home-assistant/home-assistant:stable"
    volumes:
      - /share/stefan/homeassistant/config:/config
      - /etc/localtime:/etc/localtime:ro
      - /run/dbus:/run/dbus:ro
    restart: unless-stopped
    network: host
```