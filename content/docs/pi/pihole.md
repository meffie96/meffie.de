---
title: "Pi-Hole"
---

[Pi-Hole](https://pi-hole.net/) ist ein DNS-basierter Werbefilter. Ich habe ihn nativ installiert damit ich mich nicht mit Docker und IPv6 herumschlagen muss und als DNS-Server in meiner Fritzbox eingetragen. Dazu gibt es schon genug Tutorials, deshalb wiederhole ich das hier nicht. Dabei wird auch ein cronjob angelegt (`/etc/cron.d/pihole`), welcher dafür sorgt, dass die Ad-Lists und pihole selbst regelmäßig aktualisiert werden.

## Pausieren von pihole mithilfe von homeassistant
Da ich das Webinterface von pihole nicht benötige und nicht allzu viel auf meinem Pi laufen lassen möchte, habe ich mich dagegen entschieden, den Webserver zu installieren. Allerdings wollte ich dennoch eine einfache Möglichkeit haben, pihole für ein paar Minuten zu deaktivieren, sollte es notwendig sein. Hätte ich den Webserver mitinstalliert, hätte ich die [pihole Integration](https://www.home-assistant.io/integrations/pi_hole/) nutzen können. So musste ich mir anderweitig behelfen.

### Einrichtung des Pause-Knopfs
Homeassistant bietet die Möglichkeit, ein lokales Skript mithilfe der [Shell Command Integration](https://www.home-assistant.io/integrations/shell_command/) auszuführen. Da ich jedoch Docker für meine Homeassistant-Instanz verwende, konnte ich nicht direkt auf pihole zugreifen und musste zunächst herausfinden, welche Dateien und Ordner ich in meinen Container mounten muss. Ich habe der `docker-compose.yml` noch folgende volumes hinzugefügt:

```
version: '3'
services:
  homeassistant:
    container_name: homeassistant
    image: "ghcr.io/home-assistant/home-assistant:stable"
    volumes:
      - /usr/local/bin/pihole:/usr/local/bin/pihole
      - /opt/pihole:/opt/pihole
      - /etc/pihole:/etc/pihole
```

Außerdem musste ich der `configuration.yaml` von Homeassistant folgendes hinzufügen:
```
shell_command:
  pihole_pause: /usr/local/bin/pihole disable 5m

homeassistant:
  allowlist_external_dirs:
    - "/opt"
    - "/etc/pihole"
```

Anschließend habe ich einen Button-Helper und eine Automatisierung erstellt. Immer wenn der Knopf gedrückt wird, wird der Shell-Command ausgeführt. Hier die Automatisierung:
```
alias: Disable Pihole
trigger:
  - platform: state
    entity_id:
      - input_button.disable_pihole
action:
  - service: shell_command.pihole_pause
mode: single
```


### Auslesen des aktuellen pihole-status
Damit ich in Homeassistant sehen kann, ob pihole gerade aktiv ist oder nicht, habe ich mir einen Sensor angelegt. Besonders schön ist es nicht geworden, da der Sensor entweder `BLOCKING_ENABLED=true` oder `BLOCKING_ENABLED=false` anzeigt, aber es ist besser als nichts. Dazu habe ich der `configuration.yaml` einen [Sensor](https://www.home-assistant.io/integrations/file/#sensor) hinzugefügt:
```
sensor:
  - platform: file
    file_path: /etc/pihole/setupVars.conf
    name: pihole
```
Der Sensor liest immer die letzte Zeile der Datei aus, in meinem Fall ist das zum Glück genau die die ich brauche. Man kann den Sensor auch noch etwas hübscher gestalten, indem man dem `customize`-Abschnitt in der `configuration.yaml` folgendes hinzufügt:
```
customize:
  sensor.pihole:
    icon: mdi:pi-hole
```