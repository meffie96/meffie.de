---
title: Docker auf meinem Raspberry Pi
---

## Installation
Ich habe Docker und docker-compose entgegen der Empfehlung auf [docker.com](https://docs.docker.com/engine/install/debian/) über die Paketlisten von Debian installiert. Damit bekomme ich neue Updates zwar nicht so schnell, aber die benötige ich für meinen nicht aus dem Internet erreichbaren Raspberry Pi auch nicht.

## Update von laufenden docker-compose Projekten
Ich habe mir ein kleines Skript geschrieben, welches die laufenden Docker-Container durchgeht und ihre docker-compose.yml ermittelt. Damit können die Images leicht aktualisiert und bei Bedarf neu gestartet werden. Das Skript habe ich in `/etc/cron.weekly/` abgelegt, damit es ein Mal wöchentlich ausgeführt wird. Wichtig ist, dass es keine Dateiendung hat und ausführbar ist, sonst funktioniert es nicht.

```
#!/bin/bash
# Dieses Skript geht alle *laufenden* Container durch, ermittelt den Speicherort ihrer
# docker-compose.yml und startet die Container bei Updates neu.
# Funktioniert nur bei Images, die nicht lokal gebaut werden.
# Achtung: Es werden alle in der docker-compose.yml vorhandenen Dienste gestartet.

PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:

# Alle laufenden Container
container=$(docker ps --format="{{.ID}}")

# Erstelle ein Array mit allen docker-compose files ohne Duplikate
for a_container in $container; do
    config_dir=$(docker inspect $a_container | grep "com.docker.compose.project.working_dir" | cut -d '"' -f 4)
    echo ${config_files[*]} | grep "$config_dir" &>/dev/null
    if [ $? -eq 1 ]; then
        config_files+=("$config_dir/docker-compose.yml")
    fi
done

echo "$(date '+%F %T:') Starting update of ${#config_files[@]} docker-compose projects."

# Aktualisiere Images und starte Container bei Bedarf neu
for config in ${config_files[@]}; do
    if [ -f $config ]; then
        docker-compose -f $config pull -q --ignore-pull-failures
        docker-compose -f $config up -d
    fi
done

echo "$(date '+%F %T:') Done."
```


## Monatliche Bereinigung aller nicht verwendeten Images

Lege eine neue Datei `/etc/cron.monthly/prune_images` mit dem Inhalt `docker image prune -f` an und mache sie ausführbar:
```
sudo sh -c 'echo "docker image prune -f" > /etc/cron.monthly/prune_images'
sudo chmod +x /etc/cron.monthly/prune_images
```

Der Befehl wird nun ein Mal im Monat ausgeführt und löscht alle nicht mehr von einem Container verwendeten Images.