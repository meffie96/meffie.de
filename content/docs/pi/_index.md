---
title: Raspberry Pi
---

## Installation
Die Installation von Raspberry Pi OS 64-Bit Lite (ohne GUI) habe ich mithilfe des [Raspberry Pi Imager](https://www.raspberrypi.com/software/) durchgeführt. Dort lassen sich auch direkt Nutzername und Passwort vergeben. Außerdem ist es möglich, das Kennwort fürs WLAN anzugeben und SSH zu aktivieren, sodass es nicht mehr nötig ist, den Raspberry Pi für die Ersteinrichtung an einen Monitor und Tastatur anzuschließen. So kann man sich direkt per SSH verbinden.
