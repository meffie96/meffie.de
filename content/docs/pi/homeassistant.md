---
title: "Homeassistant"
weight: 1
---

## Wecker auslesen und Licht einschalten
Mithilfe eines Templates und einer Automatisierung ist es möglich, ein paar Minuten vor dem nächsten Wecker Aktionen auszuführen, wie z.B. das Licht einzuschalten.

### 1. Template anlegen
{{< callout type="info" >}}
Zu finden unter:
> settings -> devices & services -> helpers -> create helper -> template -> binary sensor
{{< /callout>}}

{{< callout type="warning" >}}
Der Sensor muss auf das gewünschte Gerät angepasst werden, in meinem Fall ist das `sensor.gm1911_next_alarm`.
{{< /callout>}}
{{< tabs items="feste Verzögerung, variable Verzögerung" >}}
{{< tab >}}
Dieses Template wechselt seinen Status von `off` zu `on`, 5 Minuten bevor der nächste Wecker losgeht. Der Zeitpunkt kann angepasst werden, indem die `300` durch die gewünschte Anzahl von Sekunden ersetzt wird.
```
{% if not is_state('sensor.gm1911_next_alarm', 'unavailable') -%}
  {{ as_timestamp(states('sensor.gm1911_next_alarm')) - 300 < as_timestamp(now()) }}
{%- endif %}
```
{{< /tab >}}

{{< tab >}}
Es ist auch möglich, mithilfe eines zuvor angelegten Helpers auszuwählen, viele Minuten vor dem Wecker der Status auf `on` gehen soll. Dazu bietet sich ein number helper an. Dieser erscheint nach dem Anlegen als Slider im Dashboard. Mit einem `alarm` genannten Helper sieht das dann so aus:
```
{% if not is_state('sensor.gm1911_next_alarm', 'unavailable') -%}
  {{ as_timestamp(states('sensor.gm1911_next_alarm')) - int(states('input_number.alarm')) * 60 < as_timestamp(now()) }}
{%- endif %}
```
{{< /tab >}}
{{< /tabs >}}

Device class kann leer bleiben. Das Icon kann anschließend geändert werden, indem auf den neu erstellten Helper und dann auf das Zahnrad geklickt wird. Dort lässt sich auch eine Area einstellen und festlegen, ob die Entität auf dem Dashboard sichtbar sein soll.


### 2. Automatisierung anlegen
{{< callout type="info" >}}
Zu finden unter:
> settings -> automation & scenes
{{< /callout>}}

Die Automatisierung müsst ihr natürlich an eure Wünsche anpassen. Ich schalte hier das Licht in meinem Schlafzimmer ein, warte 10 Minuten und schalte es dann wieder aus.
```
alias: Wecker
trigger:
  - platform: state
    entity_id:
      - binary_sensor.alarm_stefan
    to: "on"
action:
  - service: light.turn_on
    target:
      area_id: stefans_bedroom
  - delay:
      minutes: 10
  - service: light.turn_off
    target:
      area_id: stefans_bedroom
mode: restart
```
