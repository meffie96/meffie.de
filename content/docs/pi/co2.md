---
title: CO2 Sensor am Raspberry Pi
---

Ich habe mir einen MH-Z19C Infrarot CO2 Sensor für 10,85€ besorgt und an meinen Raspberry Pi angeschlossen. Leider hat das Auslesen über die GPIO Serielle Schnittstelle nicht funktioniert, weshalb ich den CO2-Wert über den PWM-Wert realisiert habe.

## Verbindung mit homeassistant
Damit der Wert von meinem homeassistant ausgelesen werden kann, schreibe ich den aktuellen Wert minütlich mithilfe eines cronjobs in eine Datei.

Crontab von root:
```
* * * * * python -m mh_z19 --pwm | grep --only-matching --extended-regexp '[0-9]{3,4}' > /home/stefan/homeassistant/sensors/co2.txt
```

Damit der Sensor in homeassistant auftaucht, habe ich folgende Zeilen zur `configuration.yaml` hinzugefügt:
```
sensor:
  - platform: file
    file_path: /opt/sensors/co2.txt
    name: co2
    unit_of_measurement: 'ppm'

homeassistant:
  allowlist_external_dirs:
    - "/opt"

  customize:
    sensor.co2:
      icon: mdi:molecule-co2
      device_class: carbon_dioxide
```

Da ich homeassistant mit docker-compose verwende und den Ordner `/home/stefan/homeassistant/sensors/` nach `/opt/sensors` mounte, musste ich den Pfad `/opt/` zu `allowlist_external_dirs` hinzufügen.
